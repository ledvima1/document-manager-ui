import { request } from "../../helpers/index";
import { getLinkInfo } from "./utils";


export const getDocumentList = async () => {
  const token = localStorage.getItem("token");
  const url = "/documents";
  try {
    const documentList = await request(url, "GET", null, {
      Authorization: `Bearer ${token}`,
    });
    return documentList;
  } catch (error) {
    return Promise.reject(error);
  }
};

export const getFolderByURI = async (folderURI) => {
  const token = localStorage.getItem("token");
  const url = `/folders/${folderURI}?namespace=http://example.cz/Folder`;
  const folder = await request(url, "GET", null, {
    Authorization: `Bearer ${token}`,
  });
  return folder;
};

export const getSubfolders = async (url) => {
  const token = localStorage.getItem("token");

  try {
    const subfoldersList = await request(url, "GET", null, {
      Authorization: `Bearer ${token}`,
    });
    return subfoldersList;
  } catch (error) {
    return Promise.reject(error);
  }
};

export const getFiles = async (url) => {
  const token = localStorage.getItem("token");

  try {
    const files = await request(url, "GET", null, {
      Authorization: `Bearer ${token}`,
    });
    return files;
  } catch (error) {
    return Promise.reject(error);
  }
};

export const getFileInfo = async (fileName) => {
  const token = localStorage.getItem("token");

  const url = `/files/${fileName}?namespace=http://example.cz/File`;
  try {
    const fileInfo = await request(url, "GET", null, {
      Authorization: `Bearer ${token}`,
    });
    return fileInfo;
  } catch (error) {
    return Promise.reject(error);  }
};

export const getFileVersions = async (fileName) => {
  const token = localStorage.getItem("token");
  const url = `/files/${fileName}/versions?namespace=http://example.cz/File`;
  try {
    const fileVersions = await request(url, "GET", null, {
      Authorization: `Bearer ${token}`,
    });
    return fileVersions;
  } catch (error) {
    return Promise.reject(error);
  }
};

export const downloadFile = async (folder, version) => {
  const isVersionFile = typeof version !== 'undefined';
  
  let fileName = getLinkInfo(folder["@id"], 2);

  if (isVersionFile) {
    fileName = fileName.split('_')[0];
  }

  const token = localStorage.getItem("token");
  const url = isVersionFile
    ? `/files/${fileName}/content/${version}?namespace=http://example.cz/File`
    : `/files/${fileName}/content?namespace=http://example.cz/File`;
  try {

    const response = await fetch(url, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      }
    });

    const buffer = await response.arrayBuffer();
    const bytes = new Uint8Array(buffer);
    const blob = new Blob([bytes]);
    const link = document.createElement("a");
    link.href = URL.createObjectURL(blob);
    link.setAttribute(
      "download",
      `${folder["http://example.cz/fileName"]}`
    );

    document.body.appendChild(link);
    link.click();
    link.parentNode.removeChild(link);
    return buffer
  } catch (error) {
    return error;
  }
};


export const getFileContent = async (fileName, version) => {
  const token = localStorage.getItem("token");
  const url = version
    ? `/files/${fileName}/content/${version}?namespace=http://example.cz/File`
    : `/files/${fileName}/content?namespace=http://example.cz/File`;
  try {
    const fileContent = await request(url, "GET", null, {
      Authorization: `Bearer ${token}`,
    });

    return fileContent;
  } catch (error) {
    return error;
  }
};

export const getDocumentPermissions = (documentId) => {
  const token = localStorage.getItem("token");
  const url = `documents/${documentId}/permissions/user?namespace=http://example.cz/Document`;
  try {
    const userPermissions = request(url, "GET", null, {
      Authorization: `Bearer ${token}`,
    });

    return userPermissions;
  } catch (error) {
    return error;
  }
};

export const getCurrentUser = () => {
  const token = localStorage.getItem("token");
  const url =
    "https://kbss.felk.cvut.cz/authorization-service/api/v1/users/current";
  try {
    const currentUser = request(url, "GET", null, {
      Authorization: `Bearer ${token}`,
    });

    return currentUser;
  } catch (error) {
    return error;
  }
};

export const getUsers = () => {
  const token = localStorage.getItem("token");
  const url =
    "https://kbss.felk.cvut.cz/authorization-service/api/v1/tenants/current/users";
  try {
    const currentUser = request(url, "GET", null, {
      Authorization: `Bearer ${token}`,
    });

    return currentUser;
  } catch (error) {
    return error;
  }
};

export const addFile = async (entityType, folder, newFile, fileName) => {
  const token = localStorage.getItem("token");

  const url = `${entityType.toLowerCase()}s/${folder}/files?namespace=http://example.cz/${entityType}`;

  const formData = new FormData();

  formData.append("file", newFile);
  formData.append(
    "uri",
    `http://example.cz/File/${fileName.trim().replace(/\s/g, "")}.html`
  );
  formData.append("name", fileName);

  try {
    const file = await request(
      url,
      "POST",
      formData,
      {
        Authorization: `Bearer ${token}`,
      },
      true
    );
    console.log(file);
    return file;
  } catch (error) {
    return Promise.reject(error);
  }
};

export const addFolder = async (
  type,
  name,
  description,
  isRoot,
  parentFolderId = null
) => {
  const token = localStorage.getItem("token");

  const uriName = name.trim().replace(/\s/g, "");
  const typpe =
    type === "Document"
      ? "documents/"
      : `folders/${parentFolderId}${
          isRoot ? "_root" : ""
        }/subfolders?namespace=http://example.cz/Folder`;

  try {
    const folder = await request(
      `${typpe}`,
      "POST",
      {
        uri: `http://example.cz/${type}/${uriName}`,
        name: `${name}`,
        description: `${description}`,
      },
      {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      }
    );
    return folder;
  } catch (error) {
    return Promise.reject(error);
  }
};

export const addUserPermission = (documentId, permissionLevel, userURI) => {
  const token = localStorage.getItem("token");

  const url = `documents/${documentId}/permissions/user?namespace=http://example.cz/Document`;

  try {
    const newPermission = request(
      url,
      "POST",
      {
        permissionLevel: `${permissionLevel}`,
        userURI: `http://example.org/users/${userURI}`,
      },
      {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      }
    );

    return newPermission;
  } catch (error) {
    return Promise.reject(error);
  }
};

// PUT

export const updateFolder = async ({ url, id, values, type }) => {
  console.log(id);
  const token = localStorage.getItem("token");

  try {
    const updatedFolder = await request(
      url,
      "PUT",
      {
        "@id": `http://example.cz/${type}/${id}`,
        "@type": [`http://example.cz/${type}`, "http://example.cz/Node"],
        "http://example.cz/name": `${values.name}`,
        "http://example.cz/description": `${values.description}`,
      },
      {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/ld+json",
      }
    );

    return updatedFolder;
  } catch (error) {
    return Promise.reject(error);
  }
};

export const updateFile = (newData, fileName, updateType) => {
  const token = localStorage.getItem("token");

  if (updateType === "content") {
    const url = `files/${fileName}/${updateType}?namespace=http://example.cz/File`;
    const formData = new FormData();
    formData.append("file", newData);

    try {
      return request(
        url,
        "PUT",
        formData,
        {
          Authorization: `Bearer ${token}`,
        },
        true
      );
    } catch (error) {
      return Promise.reject(error);
    }
  } else {
    const url = `files/${fileName}?namespace=http://example.cz/File`;
    console.log(newData);
    console.log(fileName);
    try {
      return request(
        url,
        "PUT",
        {
          "@id": `http://example.cz/File/${fileName}`,
          "@type": ["http://example.cz/File", "http://example.cz/Node"],
          "http://example.cz/name": `${newData}`,
        },
        {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/ld+json",
        },
        false
      );
    } catch (error) {
      return Promise.reject(error);
    }
  }
};

// DELETE
export const deleteFolder = async (folderId, isRoot) => {
  const token = localStorage.getItem("token");

  const url = isRoot
    ? `documents/${folderId}?namespace=http://example.cz/Document`
    : `folders/${folderId}?namespace=http://example.cz/Folder`;
  try {
    await request(url, "DELETE", null, {
      Authorization: `Bearer ${token}`,
    });
  } catch (error) {
    return Promise.reject(error);
  }
};

export const deleteFile = async (fileName) => {
  const token = localStorage.getItem("token");
  const url = `files/${fileName}?namespace=http://example.cz/File`;

  try {
    await request(url, "DELETE", null, {
      Authorization: `Bearer ${token}`,
    });
  } catch (error) {
    return Promise.reject(error);
  }
};

export const deleteUserPermission = async (permissionId) => {
  const token = localStorage.getItem("token");
  const url = `permissions/user/${permissionId}?namespace=http://example.cz/UserPermission`;

  try {
    return await request(url, "DELETE", null, {
      Authorization: `Bearer ${token}`,
    });
  } catch (error) {
    return Promise.reject(error);
  }
};
