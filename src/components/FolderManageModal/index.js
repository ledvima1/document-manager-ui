import { TextField } from "@material-ui/core";
import React, { useState, useEffect } from "react";
import { getDocumentPermissions } from "../../features/document/api";
import { getLinkInfo } from "../../features/document/utils";
import { Loader } from "../Loader";
import { ManagePermissions } from "../ManagePermissions";
import { Modal } from "../Modal";
import { StyledManagePermissionsTitle, StyledPermissionsLevel } from "./styled";

const FolderManageModal = ({
  initialData = {},
  handleSubmit,
  onClose,
  eventType,
  folderId = null,
  handleDeleteUserPermission,
  isRoot,
  title = null,
}) => {
  const [error, setError] = useState(null);
  const [isOpenPermissionOptions, setOpenPermissionOptions] = useState(null);
  const [folderFields, setFolderFields] = useState({
    name: initialData.name || "",
    description: initialData.description || "",
    userURI: initialData.userURI || "",
    permissionLevel: initialData.permissionLevel || "",
  });
  const [userRights, setUserRights] = useState("");
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    if (folderId) {
      const documentId = getLinkInfo(folderId, 2);
      getDocumentPermissions(documentId).then((permissionsList) => {
        permissionsList.forEach((perm) => {
          if (
            perm["http://example.cz/userUri"] ===
            localStorage.getItem("currentUserUri")
          ) {
            setUserRights(perm["http://example.cz/level"]);
          }
        });
      });
    }
  }, []);

  const handleInput = (event, key) => {
    setFolderFields({ ...folderFields, [key]: event.target.value });
  };

  const onSubmit = async () => {
    try {
      setError(null);
      setLoading(true);
      await handleSubmit(folderFields);
      setLoading(false);
      onClose();
    } catch (error) {
      setLoading(false);
      setError('Such folder exists');
    }
  };
  return (
    <Modal
      handleSubmit={onSubmit}
      onClose={onClose}
      header={title || `${eventType} a folder`}
    >
      {isLoading && (
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            height: "100%",
            width: "100%",
            background: "rgba(0,0,0,0.2)",
            zIndex: 101,
            position: "absolute",
            left: "50%",
            top: "50%",
            transform: "translate(-50%, -50%)",
          }}
        >
          <Loader size="3rem" />
        </div>
      )}
      <div className="modalContent" style={{ width: "89%" }}>
        <TextField
          style={{
            width: "100%",
            marginBottom: "10px",
            marginTop: "10px",
          }}
          id="outlined-basic"
          label="Document name"
          variant="outlined"
          value={folderFields.name}
          onChange={(event) => handleInput(event, "name")}
        />
        <TextField
          style={{ width: "100%", marginBottom: "10px" }}
          id="outlined-basic"
          label="Document description"
          variant="outlined"
          value={folderFields.description}
          onChange={(event) => handleInput(event, "description")}
        />
        {error && <div style={{ color: "red" }}>{error}</div>}
        {eventType === "Edit" && isRoot && (
          <StyledPermissionsLevel>
            Your access level is {userRights}
          </StyledPermissionsLevel>
        )}

        {userRights === "SECURITY" && eventType === "Edit" && isRoot && (
          <StyledManagePermissionsTitle
            onClick={() => setOpenPermissionOptions(!isOpenPermissionOptions)}
          >
            Manage user permissions
          </StyledManagePermissionsTitle>
        )}
        {isOpenPermissionOptions && (
          <ManagePermissions
            initialData={initialData}
            folderId={folderId}
            handleDeleteUserPermission={handleDeleteUserPermission}
          />
        )}
      </div>
    </Modal>
  );
};

export { FolderManageModal };
