import styled from "styled-components";

export const StyledManagePermissionsTitle = styled.div`
  text-align: center;
  cursor: pointer;
  color: #2196f3;
`;

export const StyledPermissionsLevel = styled.div`
  margin-bottom: 10px;
  text-align: center;
`;
