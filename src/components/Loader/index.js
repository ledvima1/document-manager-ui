import React from 'react';
import { LoaderContainer } from './styled';

const Loader = ({ size }) => {
  return (
    <LoaderContainer $size={size}>
      <div className="three"></div>
      <div className="three"></div>
      <div className="three"></div>
    </LoaderContainer>
  );
};

export { Loader };
