import styled from "styled-components";

export const LoaderContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: ${({ $size }) => $size};
  height: ${({ $size }) => $size};

  .three {
    display: inline-flex;
    width: 30%;
    height: 30%;
    border-radius: 100%;
    background-color: #2196f3;
    animation: three 1.5s infinite ease-in-out both;

    &:first-child {
      animation-delay: -320ms;
    }

    &:nth-child(2) {
      animation-delay: -160ms;
    }
  }

  @keyframes three {
    0%,
    80%,
    100% {
      transform: scale(0);
    }
    40% {
      transform: scale(1);
    }
  }
`;
