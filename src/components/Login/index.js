import React from 'react';

const Login = () => {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        marginTop: "80px",
      }}
    >
      <h1>Welcome to the Document Manager</h1>
      <h2>
        Please,{" "}
        <a href="https://kbss.felk.cvut.cz/authorization-service/?tenant=http://example.org/tenants/document-manager&redirectTo=http://localhost:3000/auth">
          log in
        </a>
      </h2>
    </div>
  );
};

export { Login };
