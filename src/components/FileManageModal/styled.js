import styled from "styled-components";

export const StyledFileUploadWrapper = styled.div`
  display: flex;
  width: 80%;
  flex-direction: column;
  align-items: center;
  margin: 10px;
`;