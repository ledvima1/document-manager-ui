import React, { useState } from "react";
import { Loader } from "../Loader";
import { Modal } from "../Modal";
import { StyledFileUploadWrapper } from "./styled";
import Button from '@material-ui/core/Button';

const FileManageModal = ({ initialData = {}, handleSubmit, onClose }) => {
  const [isLoading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  
  const [fileFields, setFileFields] = useState({
    file: initialData.file || "",
    filename: initialData.filename || "",
  });
  console.log(fileFields)

  const onSubmit = async () => {
    try {
      setError(null);
      setLoading(true);
      await handleSubmit(fileFields);
      setLoading(false);
      onClose();
    } catch (error) {
      setLoading(false);
      setError('Such file exists')
    }
  };

  const handleFileUpload = (event) => {
    const _file = event.target.files[0];
    setFileFields({
      ...fileFields,
      filename: _file.name.split(".")[0],
      file: _file,
    });
  };

  const hasFile = !!fileFields.file;

  return (
    <Modal handleSubmit={onSubmit} onClose={onClose} header="Add a file" isDisabled={!hasFile}>
      {error && <div style={{ color: "red" }}>{error}</div>}
      {isLoading && (
        <div style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          height: '100%',
          width: '100%',
          background: 'rgba(0,0,0,0.2)',
          zIndex: 101,
          position: 'absolute',
          left: '50%',
          top: '50%',
          transform: 'translate(-50%, -50%)',
        }}>
          <Loader size="3rem" />
        </div>
      )}
      <StyledFileUploadWrapper>
      <div style={{marginBottom:'10px', fontSize: '17px'}}>Please, choose a file</div>
        <input
          style={{ display: 'none' }}
          id="contained-button-file"
          multiple={false}
          type="file"
          onChange={handleFileUpload}
        />
      <label htmlFor="contained-button-file">
        <Button variant="contained" component="span">
          Upload
        </Button>
      </label>
      {hasFile && (
        <div style={{ marginTop: 20 }}>
          <div>
            {" "}
            Filename:{" "}
            <input
              type="text"
              value={fileFields.filename}
              onChange={(event) =>
                setFileFields({ ...fileFields, filename: event.target.value })
              }
            />
          </div>
        </div>
      )}
      </StyledFileUploadWrapper>
    </Modal>
  );
};

export { FileManageModal };
