import React, { useState, memo, useEffect } from "react";

import { getFileInfo, downloadFile } from "../../features/document/api";
import { StyledFolderItem, StyledIcon, StyledIconWrapper } from "./styled";
import addFolderIcon from "./assets/add_folder.svg";
import addFileIcon from "./assets/add_file.svg";
import deleteFolderIcon from "./assets/delete_folder.svg";
import getFileInfoIcon from "./assets/info.svg";
import folderIcon from "./assets/folder_simple.svg";
import filledFolderIcon from "./assets/folder.svg";
import fileIcon from "./assets/file_simple.svg";
import editIcon from "./assets/edit.svg";
import downloadIcon from "../../assets/download.svg";
import { useFolderChildren, useFolderPermission } from "./hooks";
import { getLinkInfo } from "../../features/document/utils";

const FolderList = ({
  rootLevel,
  list,
  setModals,
  setOpenFileInfo,
  setFileInfo,
  isRoot = false,
  parentId = null,
  isCloseEvent,
}) => {
  return (
    <div style={{ width: "fit-content" }}>
      {list.map((document) => (
        <FolderItem
          rootLevel={rootLevel}
          key={document ? document["@id"] : ""}
          folder={document}
          isRoot={isRoot}
          setModals={setModals}
          setOpenFileInfo={setOpenFileInfo}
          setFileInfo={setFileInfo}
          parentId={parentId}
          isCloseEvent={isCloseEvent}
        />
      ))}
    </div>
  );
};

const FolderItem = memo(
  ({
    rootLevel,
    folder,
    isRoot,
    setModals,
    setOpenFileInfo,
    setFileInfo,
    parentId,
    isCloseEvent,
  }) => {
    const isFolder =
      getLinkInfo(folder["@id"], 1) === "Folder" ||
      getLinkInfo(folder["@id"], 1) === "Document";

    const { folderChilds } = useFolderChildren({ isRoot, folder, isFolder });
    const { level } = useFolderPermission({ isRoot, document: folder });

    const [isOpen, setOpen] = useState(false);
    const [_opacity, setOpacity] = useState(0);

    useEffect(() => {
      if (isRoot && isCloseEvent) {
        setOpen(false);
      }
    }, [isCloseEvent, isRoot]);

    const onMouseEnter = (e) => {
      e.stopPropagation();
      setOpacity(1);
    };

    const onMouseLeave = (e) => {
      e.stopPropagation();
      setOpacity(0);
    };

    const isFilledFolder = isFolder && folderChilds.length > 0;

    const renderFolderOrFileIcon = () => {
      if (isFolder) {
        if (isFilledFolder) {
          return <StyledIcon src={filledFolderIcon} />;
        }

        return <StyledIcon src={folderIcon} />;
      }

      return <StyledIcon src={fileIcon} />;
    };

    const openFileInfo = async () => {
      const fileInfo = await getFileInfo(getLinkInfo(folder["@id"], 2));

      setFileInfo({ ...fileInfo, userLevel: rootLevel, parentId });
      setOpenFileInfo(true);
    };

    const hasChildren = isOpen && folderChilds.length > 0;
    return (
      <div>
        <StyledFolderItem
          isOpen={hasChildren}
          onMouseOver={onMouseEnter}
          onMouseOut={onMouseLeave}
        >
          <div style={{ display: "flex" }}>
            {renderFolderOrFileIcon()}
            <div
              onClick={(event) => {
                event.stopPropagation();
                if (folderChilds.length > 0) setOpen(!isOpen);
                if (getLinkInfo(folder["@id"], 1) === "File") {
                  openFileInfo();
                }
              }}
            >
              {folder["http://example.cz/name"]}
            </div>
            {(getLinkInfo(folder["@id"], 1) === "Folder" ||
              getLinkInfo(folder["@id"], 1) === "Document") && (
              <StyledIconWrapper opacity={_opacity}>
                <StyledIcon
                  src={addFolderIcon}
                  title="Add a new folder"
                  onClick={() => {
                    if (
                      level === "READ" ||
                      level === "NONE" ||
                      rootLevel === "READ" ||
                      rootLevel === "NONE"
                    ) {
                      alert("You do not have permissions for this");
                      return;
                    }

                    setModals({
                      folder: {
                        isOpen: true,
                        folderId: null,
                        parentFolderId: folder["@id"] || null,
                        isEdit: false,
                        isRoot,
                        initialData: {},
                      },
                    });
                  }}
                />
                <StyledIcon
                  src={addFileIcon}
                  title="Add a new file"
                  onClick={() => {
                    if (
                      level === "READ" ||
                      level === "NONE" ||
                      rootLevel === "READ" ||
                      rootLevel === "NONE"
                    ) {
                      alert("You do not have permissions for this");
                      return;
                    }
                    setModals({
                      folder: {
                        isOpen: false,
                        folderId: folder["@id"],
                        parentFolderId: isRoot
                          ? null
                          : folder["http://example.cz/parentFolder"]["@id"],
                        isRoot,
                        fileAdd: true,
                        updateType: null,
                      },
                      file: {
                        isOpen: true,
                        isAdd: true,
                      },
                    });
                  }}
                />

                <StyledIcon
                  src={editIcon}
                  title="Edit a folder"
                  onClick={() => {
                    if (
                      level === "READ" ||
                      level === "NONE" ||
                      rootLevel === "READ" ||
                      rootLevel === "NONE"
                    ) {
                      alert("You do not have permissions for this");
                      return;
                    }
                    setModals({
                      folder: {
                        isOpen: true,
                        folderId: folder["@id"],
                        parentFolderId: isRoot
                          ? null
                          : folder["http://example.cz/parentFolder"]["@id"],
                        isEdit: true,
                        isRoot,
                        initialData: {
                          name: folder["http://example.cz/name"],
                          description: folder["http://example.cz/description"],
                        },
                      },
                    });
                  }}
                />
                <StyledIcon
                  src={deleteFolderIcon}
                  title="Delete a folder"
                  onClick={() => {
                    if (
                      level === "READ" ||
                      level === "NONE" ||
                      rootLevel === "READ" ||
                      rootLevel === "NONE"
                    ) {
                      alert("You do not have permissions for this");
                      return;
                    }
                    setModals({
                      folder: {
                        isOpen: false,
                        folderId: folder["@id"],
                        parentFolderId: isRoot
                          ? null
                          : folder["http://example.cz/parentFolder"]["@id"],
                        isEdit: false,
                        isDelete: true,
                        isRoot,
                        initialData: {
                          name: folder["http://example.cz/name"],
                          description: folder["http://example.cz/description"],
                        },
                      },
                      file: {
                        isFile: false,
                      },
                    });
                  }}
                />
              </StyledIconWrapper>
            )}

            {getLinkInfo(folder["@id"], 1) === "File" && (
              <StyledIconWrapper opacity={_opacity}>
                <StyledIcon
                  src={getFileInfoIcon}
                  title="Open file info"
                  onClick={openFileInfo}
                />
                <StyledIcon
                  src={downloadIcon}
                  title="Download a file"
                  onClick={async () => await downloadFile(folder)}
                />
                <StyledIcon
                  src={deleteFolderIcon}
                  title="Delete a file"
                  onClick={() => {
                    if (
                      level === "READ" ||
                      level === "NONE" ||
                      rootLevel === "READ" ||
                      rootLevel === "NONE"
                    ) {
                      alert("You do not have permissions for this");
                      return;
                    }
                    setModals({
                      folder: {
                        isOpen: false,
                        folderId: folder["@id"],
                        parentFolderId: parentId,
                        isEdit: false,
                        isDelete: true,
                        isRoot,
                        initialData: {
                          name: folder["http://example.cz/name"],
                          description: folder["http://example.cz/description"],
                        },
                      },
                      file: {
                        isFile: true,
                      },
                    });
                  }}
                />
              </StyledIconWrapper>
            )}
          </div>
          {hasChildren && (
            <FolderList
              rootLevel={level ? level : rootLevel}
              list={folderChilds}
              setOpenFileInfo={setOpenFileInfo}
              setFileInfo={setFileInfo}
              setModals={setModals}
              parentId={folder["@id"]}
            />
          )}
        </StyledFolderItem>
      </div>
    );
  }
);

export { FolderList };
