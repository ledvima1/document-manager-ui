import React, { useState, useRef, useEffect } from "react";
import dayjs from "dayjs";

import {
  StyledFileCharacteristic,
  StyledFileCharacteristicsWrapper,
  StyledFileCharacteristicTitle,
  StyledFileInfo,
  StyledFileInfoHeaderWrapper,
  StyledIcon,
  StyledVersionsHeader,
  StyledVersionsHeaderWrapper,
  StyledFileVersion,
  StyledFileInfoCross,
  StyledFileInfoHeaderText,
} from "./styled";

import downloadIcon from "../../assets/download.svg";
import addVersionIcon from "../../assets/plus_versions.svg";
import { getLinkInfo } from "../../features/document/utils";
import { downloadFile, getFileVersions } from "../../features/document/api";
import fileIcon from "../FolderList/assets/file_simple.svg";
import editIcon from "../FolderList/assets/edit.svg";
import { Loader } from "../Loader";
import { updateFile } from "../../features/document/api";
import { FileManageModal } from "../FileManageModal";
import { useQueryClient } from "react-query";

export const FileInfo = ({ fileInfo, onClose }) => {
  const [isVersionsOpen, setVersionsOpen] = useState(false);
  const [isModalOpen, setModalOpen] = useState(false);
  const [fileVersions, setFileVersions] = useState([]);
  const [isEditDisabled, setEditDisabled] = useState(true);
  const [isLoading, setIsLoading] = useState(true);
  const text = useRef(fileInfo["http://example.cz/name"]);
  const contentRef = useRef(null);
  const queryClient = useQueryClient();

  const { userLevel, parentId } = fileInfo;
  const hasPermission = !(userLevel === "READ" || userLevel === "NONE");

  const handleChange = (event) => {
    text.current = event.currentTarget.textContent;
  };

  const fetchVersions = async () => {
    try {
      setIsLoading(true);
      const _versions = await getFileVersions(getLinkInfo(fileInfo["@id"], 2));
      setFileVersions(
        _versions.sort(
          (a, b) =>
            b["http://example.cz/version"] - a["http://example.cz/version"]
        )
      );
      setIsLoading(false);
    } catch (error) {}
  };

  const onAddVersion = async (data) => {
    const filename = getLinkInfo(fileInfo["@id"], 2);
    const addedFile = await updateFile(data.file, filename, "content");
    setFileVersions((prev) =>
      [...prev, addedFile].sort(
        (a, b) =>
          b["http://example.cz/version"] - a["http://example.cz/version"]
      )
    );
  };

  const handleBlur = async () => {
    if (text.current.trim().length === 0) return;

    setEditDisabled(false);
    const currentFileName = getLinkInfo(fileInfo["@id"], 2);
    await updateFile(text.current, currentFileName, "");

    const entityName = `FolderChilds:${getLinkInfo(parentId, 2)}`;
    const prevChildrens = queryClient.getQueryData(entityName);

    if (prevChildrens) {
      queryClient.setQueryData(
        entityName,
        prevChildrens.map((child) =>
          child["@id"] === fileInfo["@id"]
            ? { ...child, "http://example.cz/name": text.current }
            : child
        )
      );
    }
  };

  const unBlur = (e) => {
    if (e.key === "Enter") {
      document.activeElement.blur();
    }
  };

  useEffect(() => {
    window.addEventListener("keypress", unBlur);

    text.current = fileInfo["http://example.cz/name"];
    fetchVersions();

    return () => window.removeEventListener("keypress", unBlur);
  }, [fileInfo["http://example.cz/name"]]);

  if (isLoading) {
    return (
      <StyledFileInfo>
        <StyledFileInfoHeaderWrapper>
          <StyledFileInfoHeaderText>File information</StyledFileInfoHeaderText>
          <StyledFileInfoCross onClick={onClose}>✖</StyledFileInfoCross>
        </StyledFileInfoHeaderWrapper>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            height: "100%",
          }}
        >
          <Loader size="2rem" />
        </div>
      </StyledFileInfo>
    );
  }

  return (
    <>
      {isModalOpen && (
        <FileManageModal
          onClose={() => setModalOpen(false)}
          handleSubmit={onAddVersion}
        />
      )}
      <StyledFileInfo>
        <StyledFileInfoHeaderWrapper>
          <StyledFileInfoHeaderText>File information</StyledFileInfoHeaderText>
          <StyledFileInfoCross onClick={onClose}>✖</StyledFileInfoCross>
        </StyledFileInfoHeaderWrapper>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            padding: "20px 0px 20px 50px",
          }}
        >
          <StyledIcon
            position="relative"
            src={fileIcon}
            style={{ transform: "scale(1.5)" }}
          />
          <StyledFileCharacteristicsWrapper
            style={{
              display: "flex",
              flexDirection: "column",
            }}
          >
            <StyledFileCharacteristic>
              <StyledFileCharacteristicTitle>
                File name:
              </StyledFileCharacteristicTitle>{" "}
              <div
                contentEditable
                dangerouslySetInnerHTML={{ __html: text.current }}
                disabled={isEditDisabled || !hasPermission}
                onBlur={handleBlur}
                ref={contentRef}
                onInput={handleChange}
                onKeyPress={(e) => {
                  if (e.key === "Enter") {
                    e.preventDefault();
                  }
                }}
              />
              <StyledIcon
                position="relative"
                src={editIcon}
                style={{ transform: "scale(0.5)" }}
                onClick={() => {
                  if (!hasPermission) {
                    alert("You do not have permissions for this");
                    return;
                  }

                  setEditDisabled(!isEditDisabled);
                  setTimeout(() => {
                    contentRef.current.focus();
                  }, 0);
                }}
              />
            </StyledFileCharacteristic>
            <StyledFileCharacteristic>
              <StyledFileCharacteristicTitle>
                File type:
              </StyledFileCharacteristicTitle>{" "}
              {fileInfo["http://example.cz/fileType"]}
            </StyledFileCharacteristic>
            <StyledFileCharacteristic>
              <StyledFileCharacteristicTitle>
                File version:
              </StyledFileCharacteristicTitle>{" "}
              {fileVersions.length}
            </StyledFileCharacteristic>
            <StyledFileCharacteristic>
              <StyledFileCharacteristicTitle>
                Date created:{" "}
              </StyledFileCharacteristicTitle>{" "}
              {dayjs(fileInfo["http://example.cz/created"]).format(
                "DD MMM YYYY"
              )}
            </StyledFileCharacteristic>
          </StyledFileCharacteristicsWrapper>
        </div>

        <StyledVersionsHeaderWrapper>
          <StyledVersionsHeader
            onClick={async () => {
              setVersionsOpen(!isVersionsOpen);
            }}
          >
            {isVersionsOpen ? "Hide" : "Show"} file versions
          </StyledVersionsHeader>
          <StyledIcon
            src={addVersionIcon}
            title="Add a new file version"
            position="absolute"
            right={0}
            top="-3px"
            transform="scale(0.6)"
            onClick={() => {
              if (!hasPermission) {
                alert("You do not have permissions for this");
                return;
              }

              setModalOpen(true);
            }}
          />
        </StyledVersionsHeaderWrapper>
        {isVersionsOpen &&
          fileVersions.map((version, key) => (
            <StyledFileVersion key={key}>
              <div>
                <div>{`Version ${version["http://example.cz/version"]}`}</div>
                <div>{version["http://example.cz/fileType"]}</div>
                <div style={{ color: "#80808087" }}>
                  {dayjs(version["http://example.cz/created"]).format(
                    "DD MMM YYYY HH:mm"
                  )}
                </div>
              </div>
              <StyledIcon
                right="5%"
                top="40%"
                src={downloadIcon}
                onClick={async () => await downloadFile(version, version["http://example.cz/version"])}
              />
            </StyledFileVersion>
          ))}
      </StyledFileInfo>
    </>
  );
};
