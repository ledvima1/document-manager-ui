import styled from "styled-components";

export const StyledFileInfo = styled.div`
  position: absolute;
  height: 450px;
  overflow: auto;
  overflow-x: hidden;
  z-index: 10;
  right: 10%;
  top: 30%;
  width: 30%;
  border: 1px solid #8080803b;
  border-radius: 25px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const StyledFileInfoHeaderWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 20px;
  background: #2196f3;
  width: 100%;
  text-align: center;
  box-shadow: rgb(189 189 189) 5px 5px 10px -1px;
`;

export const StyledFileVersion = styled.div`
  border: solid 1px #80808029;
  border-radius: 10px;
  padding: 15px;
  margin: 5px;
  width: 80%;
  position: relative;
`;

export const StyledFileInfoCross = styled.div`
  color: white;
  margin-right: 5%;
  cursor: pointer;
  transform: scale(1.3);
`;

export const StyledFileInfoHeaderText = styled.div`
  margin: 0;
  margin-left: 33%;
  color: white;
  font-size: 20px;
  font-weight: 800;
`;

export const StyledFileCharacteristicsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 15px 40px;
`;

export const StyledFileCharacteristic = styled.div`
  display: flex;
  align-items: center;
`;

export const StyledFileCharacteristicTitle = styled.div`
  margin-right: 5px;
  font-weight: 700;
`;

export const StyledVersionsHeaderWrapper = styled.div`
  position: relative;
  width: 80%;
  display: flex;
  justify-content: center;
`;

export const StyledVersionsHeader = styled.div`
  width: 40%;
  margin: 0;
  color: #2196f3;
  cursor: pointer;
  margin-bottom: 10px;
  text-align: center;
`;

export const StyledIcon = styled.img`
  height: 30px;
  width: 30px;
  position: absolute;
  right: ${(props) => props.right};
  top: ${(props) => props.top};
  position: ${(props) => props.position};
  transform: ${(props) => props.transform};
  cursor: pointer;
`;
