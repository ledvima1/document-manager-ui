import React from "react";
import { Route, Switch } from "react-router-dom";
import DocumentList from "./DocumentList";
import { TokenHandler } from "./components/TokenHandler";

export const Routes = () => {
  return (
    <Switch>
      <Route path="/" exact component={DocumentList} />
      <Route path="/auth" exact component={TokenHandler} />;
    </Switch>
  );
};
