import React, { useEffect, useState } from "react";
import { FolderList } from "./components/FolderList";
import { Header } from "./components/Header";
import { Container, StyledDocumentTreeWrapper } from "./styled";
import { Icon } from "./components/Icon";
import { FileInfo } from "./components/FileInfo";
import { FolderManageModal } from "./components/FolderManageModal";
import { Login } from "./components/Login";
import {
  useDocuments,
  useAddRootFolder,
  useEditFolder,
  useDelete,
  useAddFile,
  useUpdateFile,
} from "./features/document/hooks";
import { FileManageModal } from "./components/FileManageModal";
import { Modal } from "./components/Modal";
import { getLinkInfo } from "./features/document/utils";
import { deleteUserPermission, getCurrentUser } from "./features/document/api";
import { Loader } from "./components/Loader";
import plusIcon from './assets/plus_blue.svg';

const initModalState = {
  title: null,
  folder: {
    isOpen: false,
    folderId: null,
    parentFolderId: null,
    isRoot: false,
    isEdit: false,
    current: null,
    isDelete: false,
    initialData: {},
    fileAdd: false,
    file: null,
    fileName: null,
    updateType: null,
    fileInfo: "",
    userLevel: "",
  },
};

function DocumentList() {
  const [modals, setModals] = useState(initModalState);
  const [isOpenFileInfo, setOpenFileInfo] = useState(false);
  const [isCloseEvent, setCloseEvent] = useState(false);

  const [fileInfo, setFileInfo] = useState();

  const onClose = () => setModals(initModalState);

  const { documents, loading, error } = useDocuments();
  const { addRootFolder } = useAddRootFolder({ modals, onClose });
  const { editFolder } = useEditFolder({ modals, onClose });
  const { handleDeleteEntity } = useDelete({ modals, onClose });
  const { handleAddFileFetcher } = useAddFile({ modals, onClose });
  const { handleUpdateFileFetcher } = useUpdateFile({ modals, onClose });

  useEffect(() => {
    getCurrentUser().then((user) =>
      localStorage.setItem("currentUserUri", user["uri"])
    );
  }, []);

  const handleDeleteUserPermission = (id) => {
    const permissionId = getLinkInfo(id, 2);
    deleteUserPermission(permissionId);
  };

  const isAuthorized = !!localStorage.getItem("token");

  const onCloseFolders = () => {
    setCloseEvent(true);
    setTimeout(() => setCloseEvent(false, 0));
  }

  if ((loading || !documents) && !error) {
    return (
      <div style={{
        width: '100%',
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}>
        <Loader size="4rem" />
      </div>
    );
  }

  if (!isAuthorized || (error && error.statusCode === 502)) {
    return (
      <Container>
        <Header isAuthorized={false} />
        <Login />
      </Container>
    );
  }

  if (error) {
    return <div>Error</div>;
  }

  return (
    <Container>
      <Header isAuthorized={isAuthorized} onCloseFolders={onCloseFolders} />

      <div>
        <div style={{ display: "flex", alignItems: "center" }}>
          <h1
            style={{
              marginLeft: "10%",
              display: "flex",
              alignItems: "center",
              position: "relative",
            }}
          >
            Documents
          </h1>
          <Icon
            left="20px"
            position='relative'
            src={plusIcon}
            title="Add a new document"
            onClick={() => setModals({
              ...initModalState,
              title: "Add a document",
              folder: {
                current: null,
                isOpen: true,
                folderId: null,
                parentFolderId: null,
                isRoot: true,
                isEdit: false,
                initialData: {},
                updateType: null,
                userLevel: "",
              },
              file: {
                isOpen: false,
                isFile: false,
                isAdd: false,
                isInfo: false,
                initialData: {},
              },
            })}
          />
        </div>

        {modals.folder.isOpen && (
          <FolderManageModal
            onClose={onClose}
            handleSubmit={modals.folder.isEdit ? editFolder : addRootFolder}
            initialData={modals.folder.initialData}
            eventType={modals.folder.isEdit ? "Edit" : "Add"}
            folderId={modals.folder.folderId}
            handleDeleteUserPermission={handleDeleteUserPermission}
            isRoot={modals.folder.isRoot}
            title={modals.title}
          />
        )}

        {modals.folder.isDelete && (
          <Modal
            onClose={onClose}
            handleSubmit={async (data) => {
              setOpenFileInfo(false);
              await handleDeleteEntity(data);
              onClose();
            }}
            isDelete={modals.folder.isDelete}
            header="Delete the entity"
          >
            <div>Do you want to delete this entity?</div>
          </Modal>
        )}

        {/* FILE */}

        {modals.folder.fileAdd && (
          <FileManageModal
            onClose={onClose}
            handleSubmit={
              !modals.folder.updateType
                ? handleAddFileFetcher
                : handleUpdateFileFetcher
            }
            initialData={modals.folder.initialData}
          />
        )}

        {isOpenFileInfo && (
          <FileInfo
            fileInfo={fileInfo}
            onClose={() => setOpenFileInfo(false)}
          />
        )}

        <StyledDocumentTreeWrapper>
          <FolderList
            list={documents}
            setModals={setModals}
            setOpenFileInfo={setOpenFileInfo}
            setFileInfo={setFileInfo}
            isRoot={true}
            isCloseEvent={isCloseEvent}
          />
        </StyledDocumentTreeWrapper>
      </div>
    </Container>
  );
}

export default DocumentList;
